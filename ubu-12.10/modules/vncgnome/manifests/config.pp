class vncgnome::config ($user, $group, $homedir = undef) {
    file { "$homedir/.vnc/passwd":
        source => "puppet:///modules/vncgnome/vncpasswd",
        owner => "$user",
        group => "$group",
        mode => '0600',
        ensure => present,
    }
    file { "$homedir/.vnc/xstartup":
        source => [ 
            "puppet:///modules/vncgnome/xstartup.$operatingsystem",
            "puppet:///modules/vncgnome/xstartup",
        ],
        mode => "0755",
        owner => "$user",
        group => "$group",
        ensure => present,
    }
}
