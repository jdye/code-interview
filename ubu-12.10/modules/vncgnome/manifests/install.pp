class vncgnome::install ($user, $homedir = undef) {
    case $::operatingsystem {
        Ubuntu: {
            $package_list = [ 'vnc4server', 'ubuntu-gnome-desktop' ]
        }
        default: {
            fail("${::operatingsystem} not supported by module ${module_name}")
        }
    }

    package { $package_list: 
        ensure => installed,
    }

    file { "$homedir/.vnc/":
        ensure => directory,
        owner => "$user",
        mode => '0755',
    }

    file { '/etc/init/vncserver.conf':
        owner => 'root',
        group => 'root',
        mode => '0755',
        ensure => present,
        content => template('vncgnome/vncserver.conf'),
    }

    if $::operatingsystem == 'Ubuntu' {
        # disable upstart service
        file { '/etc/init/gdm.override':
            ensure => present,
            owner => 'root',
            group => 'root',
            mode => '0600',
            content => "manual\n",
        }
    }
}
