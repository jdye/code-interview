class vncgnome ($user, $group, $homedir = undef) {
    if ($homedir == undef) {
      if ($user == 'root') {
        $USERHOME = '/root'
      } else {
        $USERHOME = "/home/$user"
      }
    } else {
      $USERHOME = $homedir
    }

    class { 'vncgnome::install':
        user => $user,
        homedir => $USERHOME,
    }

    class { 'vncgnome::config':
        user => $user,
        group => $group,
        homedir => $USERHOME,
    }

    class { 'vncgnome::fix': 
        user => $user,
        group => $group,
        homedir => $USERHOME,
    }
    
    class { 'vncgnome::service': }

    Class['vncgnome::install'] -> Class['vncgnome::config'] -> Class['vncgnome::fix'] -> Class['vncgnome::service']
}
