class vncgnome::fix ($user, $group, $homedir) {
    file { [ "$homedir/.config", "$homedir/.config/autostart" ]:
        ensure => directory,
        owner => "$user", 
        group => "$group", 
        mode => '0700',
    }
    
    file { "/usr/bin/stupid-vnc-fix":
        owner => 'root',
        group => 'root',
        source => 'puppet:///modules/vncgnome/stupid-vnc-fix',
        mode => '0755',
        notify => Service['vncserver'],
    }

    file { "fix-desktop-file":
        path => "$homedir/.config/autostart/stupid-vnc-fix.desktop",
        owner => "$user",
        group => "$group",
        require => File["$homedir/.config/autostart"],
        source => 'puppet:///modules/vncgnome/stupid-vnc-fix.desktop',
        mode => '0644',
        notify => Service['vncserver'],
    }
}
