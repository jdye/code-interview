class vncgnome::service {
    service { 'vncserver':
        ensure => running,
        enable => true,
        provider => 'upstart',
    }
}
