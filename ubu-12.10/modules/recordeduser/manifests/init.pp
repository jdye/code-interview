class recordeduser ($user, $group) {
    case $::operatingsystem {
        Ubuntu: {
            $package_list = [ 'bsdutils' ]
            $group_list = ['users', 'sudo', 'adm', 'video']
        }
        default: {
            fail("${::operatingsystem} not supported by module ${module_name}")
        }
    }      

    user { "$user":
        shell => '/usr/bin/scriptshell',
        ensure => 'present',
        home => "/home/$user",
        managehome => true,
        groups => $group_list,
        gid => "$group",
        require => Group["$group"],
    }

    group { "$group":
        ensure => 'present',
    }

    package { $package_list:
        ensure => installed,
    }

    file { [ '/logs/', '/logs/script/' ]:
        ensure => directory,
        mode => '1777',
    }

    file { '/usr/bin/scriptshell':
        owner => 'root',
        group => 'root',
        ensure => present,
        source => 'puppet:///modules/recordeduser/scriptshell',
        mode => '0755',
    }

    file { "/home/$user/.bashrc":
        owner => "$user",
        group => "$group",
        source => [
            "puppet:///modules/recordeduser/bashrc.${::operatingsystem}",
            "puppet:///modules/recordeduser/bashrc",
        ],
        mode => '0644',
        ensure => present,
    }
}
