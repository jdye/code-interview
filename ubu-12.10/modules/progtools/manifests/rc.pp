class progtools::rc ($user, $group, $homedir) {
    File {
        owner => "$user",
        group => "$group",
        mode => "0644",
    }

    # vimrc
    file { "$homedir/.vimrc":
        source => 'puppet:///modules/progtools/vimrc',
    }

    # screenrc
    file { "$homedir/.screenrc":
        source => 'puppet:///modules/progtools/screenrc',
    }   
}
