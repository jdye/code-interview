class progtools::packages {
    case $::operatingsystem {
        Ubuntu: {
            $package_list = [ 'firefox', 'nodejs', 'vim', 'emacs', 'nano', 'lynx', 'apache2-utils', 'dstat', 'g++', 'libc6-dev', 'python', 'python-pudb', 'python-eventlet', 'openjdk-6-jdk', 'openjdk-7-jdk', 'eclipse', 'terminator', 'screen', 'netcat-openbsd', 'netcat-traditional', 'git', 'mercurial', 'haskell-platform', 'erlang', 'perl', 'perl-debug', 'php5', 'ruby', 'sysstat', 'tcpdump', 'ngrep', 'iproute', 'gawk', 'python-pip', 'python-dev' ]
        }
        default: {
            fail("${::operatingsystem} not supported by ${module_name}")
        }
    }
    
    package { $package_list:
        ensure => present,
    }
}
