class progtools ($homedir, $user, $group) {
    class { progtools::packages:
    }

    class { progtools::rc:
        homedir => $homedir,
        user => $user,
        group => $group,
    }
    
    Class['progtools::packages'] -> Class['progtools::rc']
}
