$user = 'candidate'
$group = 'candidate'

class { 'recordeduser': 
    user => $user,
    group => $group,
}
class { 'vncgnome':
    user => $user,
    group => $group,
}
class { 'progtools':
    homedir => "/home/$user",
    user => $user,
    group => $group,
}

Class['recordeduser'] -> Class['vncgnome'] -> Class['progtools']
